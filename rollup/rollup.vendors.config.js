/*
    Compiles the vendors bundle.
*/
export default {
    entry: 'node_modules/vue/dist/vue.min.js',
    dest: 'dist/vendors.min.js',
    format: 'iife'
}