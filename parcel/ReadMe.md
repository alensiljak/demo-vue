# Parcel Demo

This version is using Parcel to compile the JavaScript, the components, and package the application.

Since Parcel requires no configuration, there is actually not that much to demonstrate in that area.

Another good example to demonstrate, though, is the router.

BLOCKER ISSUE: https://github.com/parcel-bundler/parcel/issues/2222

## References

- [Parcel](https://parceljs.org/)
