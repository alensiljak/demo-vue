import Vue from 'vue'
// import AppComponent from './components/AppComponent.vue'
import router from './router'

Vue.config.productionTip = false

// 4. Create and mount the root instance.
// Make sure to inject the router with the router option to make the
// whole app router-aware.
const app = new Vue({
    router
  }).$mount('#app')

// const app = new Vue({
//     el: '#app',
//     router
//     render: h => h()
// })

// import App from './App'
// router.start(App, '#app');

// Blank one, using only the component. No router.
// const app = new Vue({
//     el: '#app'
// });

// new Vue({
//     render: h => h(AppComponent)
//   }).$mount('#app')