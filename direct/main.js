import TodoItem from './todo-item.js';

var app = new Vue({
    el: '#app',
    data: {
        message: 'Hello Vue!',
        groceryList: [
            { id: 0, text: 'Vegetables' },
            { id: 1, text: 'Cheese' },
            { id: 2, text: 'Whatever else humans are supposed to eat' }
          ]
    },
    components: {
        'my-component': httpVueLoader('SampleComponent.vue')
      }
});

var app2 = new Vue({
    el: '#app-2',
    data: {
        message: 'You loaded this page on ' + new Date().toLocaleString()
    }
});
