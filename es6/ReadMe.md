# Direct

This solution uses ES6 features to achieve the goal similar to that of webpack.

## References

- [Goodbye webpack: Building Vue.js Applications Without webpack](https://markus.oberlehner.net/blog/goodbye-webpack-building-vue-applications-without-webpack)
- [import](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/import)
