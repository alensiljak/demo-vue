# test-vue

A very basic project with Vue.js. Demoing the capabilities and basic setup.

There are now several options available:

- rollup: using rollup compiler;
- parceler: zero-configuration option using Parceler;
- webpack: the solution using single-file components and webpack, single-page application with router;
- direct: using Vue.js in each HTML page;
- es6: using EcmaScript 6 features like code splitting and modules, without build tools;
